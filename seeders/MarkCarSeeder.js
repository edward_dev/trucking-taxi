const mongoose = require('mongoose');
const MarkCar = require('../models/MarkCar');

exports.MarkCarSeeder = function () {
    let types = ['Марка 1', 'Марка 2', 'Марка 3', 'Марка 4', 'Марка 5'];

    for(let i in types) {
        let obj = new MarkCar ({
            _id: new mongoose.Types.ObjectId(),
            title: types[i]
        });

        obj.save(error => {
            if (error) throw error;
            console.log('OK');
        });
    }
};