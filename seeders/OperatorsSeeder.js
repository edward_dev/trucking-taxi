const mongoose = require('mongoose');
const Operator = require('../models/Operator');

exports.OperatorSeeder = function () {
    let data = [
        {full_name: 'Test Test Test', phone: '+7777777', email: 'test@gmail.com'},
        {full_name: 'Second Second Second', phone: '+987654321', email: 'second@gmail.com'},
        {full_name: 'Operator Operator Operator', phone: '+123456789', email: 'user@gmail.com'}
    ];
    for(let i in data) {
        let obj = new Operator({
            _id: new mongoose.Types.ObjectId(),
            full_name: data[i].full_name,
            phone: data[i].phone,
            email: data[i].email,
        });

        obj.save(error => {
            if (error) throw error;

            console.log('OK')
        });
    }
}