const mongoose = require('mongoose');
const Driver = require('../models/Driver');

exports.DriverSeeder = function () {
    let data = [
        {full_name: 'd1 d1 d1', phone: '+123456789'},
        {full_name: 'd2 d2 d2', phone: '+987654321'},
        {full_name: 'd3 d3 d3', phone: '+1234567890'}
    ];
    for(let i in data) {
        let obj = new Driver({
            _id: new mongoose.Types.ObjectId(),
            full_name: data[i].full_name,
            phone: data[i].phone
        });

        obj.save(error => {
            if (error) throw error;

            console.log('OK')
        });
    }
};