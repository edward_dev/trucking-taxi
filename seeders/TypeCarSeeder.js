const mongoose = require('mongoose');
const TypeCar = require('../models/TypeCar');

exports.TypeCarSeeder = function () {
    let types = ['Тип 1', 'Тип 2', 'Тип 3', 'Тип 4', 'Тип 5'];

    for(let i in types) {
        let obj = new TypeCar ({
            _id: new mongoose.Types.ObjectId(),
            title: types[i]
        });

        obj.save(error => {
            if (error) throw error;
            console.log('OK');
        });
    }
};