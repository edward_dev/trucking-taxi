const mongoose = require('mongoose');
const Order = require('../models/Order');

exports.OrderSeeder = function () {
    let data = [
        {
            _id: new mongoose.Types.ObjectId(),
            source: {
                operator: new mongoose.Types.ObjectId("5cfbfedb21562c0d0c0fd3cd")
                // type_creator: 'operator'
            },
            phone: '+7777777123',
            place_from: {
                lat: 58.456,
                lon: 61.776,
                address: "Китайская республика, город Пекин"
            },
            place_to: {
                lat: 78.456,
                lon: 62.776,
                address: "Чечня"
            },
            length: Math.random() * (500 - 400) + 400,
            price: {
                preliminary: Math.random() * (100 - 50) + 50
            },
            time: Math.random() * (700 - 500) + 500
        }
    ];
    for(let i in data) {
        let obj = new Order(data[i]);

        obj.save(error => {
            if (error) throw error;

            console.log('OK')
        });
    }
}