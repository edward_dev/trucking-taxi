const mongoose = require('mongoose');
const ColorCar = require('../models/ColorCar');

exports.ColorCarSeeder = function () {
    let types = ['Цвет 1', 'Цвет 2', 'Цвет 3', 'Цвет 4', 'Цвет 5'];

    for(let i in types) {
        let obj = new ColorCar ({
            _id: new mongoose.Types.ObjectId(),
            title: types[i]
        });

        obj.save(error => {
            if (error) throw error;
            console.log('OK');
        });
    }
};