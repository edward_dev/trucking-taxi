const mongoose = require('mongoose');
const Schema = mongoose.Schema;
// const md5 = require('md5');
// const validator = require('validator');
// const mongodbErrorHandler = require('mongoose-mongodb-errors');
// const passportLocalMongoose = require('passport-local-mongoose');
mongoose.Promise = global.Promise;

let operator_schema = Schema({
    _id: Schema.Types.ObjectId,
    full_name: {
        type: String,
        trim: true
    },
    phone: {
        type: String,
        trim: true
    },
    email: {
        type: String,
        trim: true,
        unique: true
    }
});

let Operator = mongoose.model('Operator', operator_schema);

module.exports = Operator;