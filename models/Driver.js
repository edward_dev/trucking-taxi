const mongoose = require('mongoose');
const Schema = mongoose.Schema;
mongoose.Promise = global.Promise;

let driver_schema = Schema({
    _id: Schema.Types.ObjectId,
    full_name: {
        type: String,
        trim: true
    },
    phone: {
        type: String,
        trim: true
    },
    no_smoking: {
        type: Boolean,
        default: true
    },
    no_alcohol: {
        type: Boolean,
        default: true
    }
});

let Driver = mongoose.model('Driver', driver_schema);

module.exports = Driver;