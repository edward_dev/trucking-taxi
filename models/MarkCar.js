const mongoose = require('mongoose');
const Schema = mongoose.Schema;

let mark_car_schema = Schema({
    _id: Schema.Types.ObjectId,
    title: {
        type: String,
        required: true
    },
    created_at: {
        type: Date,
        default: Date.now
    }
});

let MarkCar = mongoose.model('MarkCar', mark_car_schema);

module.exports = MarkCar;