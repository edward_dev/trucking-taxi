const mongoose = require('mongoose');
const Schema = mongoose.Schema;
mongoose.Promise = global.Promise;

let order_schema = Schema({
    _id: Schema.Types.ObjectId,
    driver: {
        type: Schema.Types.ObjectId,
        ref: 'Driver'
    },
    // Расстояние до пункта назначения
    length: {
        type: Number,
        default: 0.0
    },
    // Информация о том, кто создал заявку, клиент, приложение или оператор
    source: {
        type_creator: {
            required: true,
            type: String,
            enum: ['mobile_application', 'operator', 'client'],
            default: 'operator'
        },
        operator: {
            type: Schema.Types.ObjectId,
            ref: 'Operator'
        }
    },
    // Номер клиента, который совершил заказ
    phone: {
        type: String,
        trim: true
    },
    time: {
        type: Number,
        default: 0.0
    },
    price: {
        // Предварительная цена
        preliminary: {
            type: Number,
            default: 0.0
        },
        // Фактическая цена
        actual: {
            type: Number,
            default: 0.0
        }
    },
    place_from: {
        lat: {
            type: Number
        },
        lon: {
            type: Number
        },
        address: {
            type: String
        }
    },
    place_to: {
        lat: {
            type: Number
        },
        lon: {
            type: Number
        },
        address: {
            type: String
        }
    },
    status: {
        type: Number,
        default: 1
    },
    created_at: {
        type: Date,
        default: Date.now
    }
});

let Order = mongoose.model('Order', order_schema);

module.exports = Order;