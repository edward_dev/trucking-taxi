const mongoose = require('mongoose');
const Schema = mongoose.Schema;

let color_car_schema = Schema({
    _id: Schema.Types.ObjectId,
    title: {
        type: String,
        required: true
    },
    created_at: {
        type: Date,
        default: Date.now
    }
});

let ColorCar = mongoose.model('ColorCar', color_car_schema);

module.exports = ColorCar;