const mongoose = require('mongoose');
const Schema = mongoose.Schema;

let type_car_schema = Schema({
    _id: Schema.Types.ObjectId,
    title: {
        type: String,
        required: true
    },
    created_at: {
        type: Date,
        default: Date.now
    }
});

let TypeCar = mongoose.model('TypeCar', type_car_schema);

module.exports = TypeCar;