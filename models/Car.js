const mongoose = require('mongoose');
const Schema = mongoose.Schema;

let car_schema = Schema({
    _id: Schema.Types.ObjectId,
    color: {
        type: Schema.Types.ObjectId,
        ref: 'ColorCar'
    },
    type_car: {
        type: Schema.Types.ObjectId,
        ref: 'TypeCar'
    },
    mark: {
        type: Schema.Types.ObjectId,
        ref: 'MarkCar'
    },
    plate_number: {
        type: String,
        required: true
    },
    // Позывной
    nickname: {
        type: String,
        default: '001'
    },
    created_at: {
        type: Date,
        default: Date.now
    }
});

let Car = mongoose.model('Car', car_schema);

module.exports = Car;