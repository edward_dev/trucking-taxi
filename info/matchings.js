exports.OrderSources = function list(){
    return {
        client_website: 1, // Заказ создался клиентом
        mobile_application: 2, // Заказ создался мобильным приложением
        operator_website: 3 // Заказ создался оператором
    }
};