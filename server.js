const mongoose = require('mongoose');
const WebSocket = require('ws');

const TypeCar = require('./models/TypeCar');
const MarkCar = require('./models/MarkCar');
const ColorCar = require('./models/ColorCar');
const Car = require('./models/Car');
const Operator = require('./models/Operator');
const Driver = require('./models/Driver');
const Order = require('./models/Order');

const server = new WebSocket.Server({port: 7001});

// Seeders
const DriverSeeder = require('./seeders/DriverSeeder').DriverSeeder;
const OrderSeeder = require('./seeders/OrderSeeder').OrderSeeder;

mongoose.connect('mongodb+srv://edward:123456qwerty@cluster0-6qm8a.mongodb.net/test', {useNewUrlParser: true});

global.USERS = {
    operators: [],
    drivers: []
};

// OrderSeeder();

// тут храним список всех методов, которые будет вызывать клиент
const handlers = {

    // Auth
    attemptAuth(ws, request) {

        if (typeof request === 'object') {
            // Запоминаем по какому соединению какой пользователь аутентифицирован
            ws.info = {};

            if (request.options.hasOwnProperty('type')) {

                if (request.options.type === 'operator') {

                    if(request.options.hasOwnProperty('email')) {

                        Operator.findOne({email: request.options.email}, '_id ', (error, data) => {
                            if(error) throw error;

                            if (data) {

                                ws.send(JSON.stringify({
                                    action: 'authSuccess',
                                    data: {
                                        _id: data._id
                                    }
                                }));

                                handlers.authOperator(ws, request);
                            }
                        });
                    }
                }

                if (request.options.type === 'driver') {

                    if(request.options.hasOwnProperty('phone')) {

                        Driver.findOne({phone: request.options.phone}, '_id ', (error, data) => {
                            if(error) throw error;

                            if (data) {

                                ws.send(JSON.stringify({
                                    action: 'authSuccess',
                                    data: {
                                        _id: data._id
                                    }
                                }));

                                handlers.authDriver(ws, request);
                            }
                        });
                    }
                }
            }
        }
    },

    authOperator(ws, request) {

        if (typeof request === 'object') {

            // Запоминаем по какому соединению какой пользователь аутентифицирован
            ws.info = {};

            if (request.options.hasOwnProperty('email')) {

                ws.info.email = request.options.email;
                ws.info.type = request.options.type;

                // В глобальный массив добавляем обьект оператора
                global.USERS.operators.push({email: request.options.email});

                // Оповещаем всех подключенных клиентов о том, что на сайт вошел какой-то оператор
                server.clients.forEach(client => {
                    client.send(JSON.stringify({
                        action: 'authOperator',
                        data: {
                            count_operators_online: global.USERS.operators.length,
                        }
                    }));

                })
                
            }

        }
    },

    authDriver(ws, request) {

        if (typeof request === 'object') {

            // Запоминаем по какому соединению какой пользователь аутентифицирован
            ws.info = {};

            if (request.options.hasOwnProperty('phone')) {

                ws.info.phone = request.options.phone;
                ws.info.type = request.options.type;

                // В глобальный массив добавляем обьект оператора
                global.USERS.drivers.push({phone: request.options.phone});

                // Оповещаем всех подключенных клиентов о том, что на сайт вошел какой-то оператор
                server.clients.forEach(client => {
                    client.send(JSON.stringify({
                        action: 'authDriver',
                        data: {
                            count_drivers_online: global.USERS.drivers.length,
                        }
                    }));

                })

            }

        }
    },

    // Get info from MongoDB
    getAllTypesCars(ws, request) {
        TypeCar.find({}, '_id title ', (error, data) => {
            if(error) throw error;

            // Отправляем данные только тому, кто запросил
            ws.send(JSON.stringify({
                action: 'getAllTypesCars',
                data: {
                    car_types: data
                }
            }));
        });
    },

    getAllColorsCars(ws, request) {
        ColorCar.find({}, '_id title ', (error, data) => {
            if(error) throw error;

            // Отправляем данные только тому, кто запросил
            ws.send(JSON.stringify({
                action: 'getAllColorsCars',
                data: {
                    car_colors: data
                }
            }));
        });
    },

    getAllMarksCars(ws, request) {
        MarkCar.find({}, '_id title ', (error, data) => {
            if(error) throw error;

            // Отправляем данные только тому, кто запросил
            ws.send(JSON.stringify({
                action: 'getAllMarksCars',
                data: {
                    car_marks: data
                }
            }));
        });
    },

    getCountDriversOnline(ws, request) {
        ws.send(JSON.stringify({
            action: 'getCountDriversOnline',
            data: {
                count_drivers_online: global.USERS.drivers.length,
            }
        }));
    },

    getCountOrdersPerToday(ws, request) {

        let date = new Date();

        Order.find({created_at: {$gte: new Date(date.getFullYear(), date.getMonth(), date.getDate()),
                $lte: new Date(date.getFullYear(), date.getMonth(), date.getDate()+1)}})
            .exec((error, data) => {
                if(error) throw error;

                // Отправляем данные только тому, кто запросил
                ws.send(JSON.stringify({
                    action: 'getCountOrdersPerToday',
                    data: {
                        count_orders_per_today: data.length
                    }
                }));
            });
    },

    callToOperator(ws, request) {
        server.clients.forEach(client => {

            if(client.hasOwnProperty('info')) {

                if (client.info.hasOwnProperty('email') && client.info.email === request.options.email)

                    client.send(JSON.stringify({
                        action: 'callToOperator'
                    }));
            }
        });
    },

    createCar(ws, request) {

        if (typeof request === 'object') {

            if (request.options.hasOwnProperty('color') &&
                request.options.hasOwnProperty('type_car') &&
                request.options.hasOwnProperty('mark') &&
                request.options.hasOwnProperty('plate_number')) {

                let color = null;
                let type_car = null;
                let mark = null;
                let plate_number = request.options.plate_number;

                ColorCar.findById(request.options.color, '_id ')
                    .then((data) => {

                        if(data) {
                            color = data;
                        }

                        TypeCar.findById(request.options.type_car, '_id ')
                            .then(data => {

                                if(data) {
                                    type_car = data;
                                }

                                MarkCar.findById(request.options.mark, '_id ')
                                    .then(data => {

                                        if(data) {
                                            mark = data;
                                        }

                                        if(color && type_car && mark && plate_number) {

                                            let newCar = new Car({
                                                _id: new mongoose.Types.ObjectId(),
                                                color: color._id,
                                                type_car: type_car._id,
                                                mark: mark._id,
                                                plate_number: plate_number
                                            });

                                            newCar.save(error => {
                                                if(error) throw error;

                                                ws.send(JSON.stringify({
                                                    action: 'createCar',
                                                    data: {
                                                        _id: newCar._id
                                                    }
                                                }));
                                            })
                                        }
                                    })

                            })
                    })
                    .catch(error => {
                        console.error("Какая-то ошибка в запросе");
                    });

            }
        }
    },

    createDriver(ws, request) {

        if (typeof request === 'object') {

            if (request.options.hasOwnProperty('full_name') &&
                request.options.hasOwnProperty('phone')) {

                let full_name = request.options.full_name;
                let phone = request.options.phone;

                let newDriver = new Driver({
                    _id: new mongoose.Types.ObjectId(),
                    full_name: full_name,
                    phone: phone,
                });

                newDriver.save(error => {
                    if(error) throw error;

                    ws.send(JSON.stringify({
                        action: 'createDriver',
                        data: {
                            _id: newDriver._id
                        }
                    }));
                })
            }
        }
    },

    getAllOrders(ws, request) {

        Order.find({})
            .populate('source.operator')
            .exec((error, data) => {
            if(error) throw error;

            // Отправляем данные только тому, кто запросил
            ws.send(JSON.stringify({
                action: 'getAllOrders',
                data: {
                    orders: data
                }
            }));
        });
    },

    getAllDrivers(ws, request) {

        Driver.find({})
            .exec((error, data) => {
            if(error) throw error;

            // Отправляем данные только тому, кто запросил
            ws.send(JSON.stringify({
                action: 'getAllDrivers',
                data: {
                    drivers: data
                }
            }));
        });
    },

    // Обновить пробег у заказа
    updateMileage(ws, request) {

        if (typeof request === 'object') {
            if (request.options.hasOwnProperty('order_id') && request.options.hasOwnProperty('mileage')) {

            }
        }
    },

};

// Событие когда клиент установил соединение с SOCKET
server.on('connection', ws => {

    ws.on('message', message => {
        let msg = JSON.parse(message);

        if (msg.hasOwnProperty('method') && handlers.hasOwnProperty(msg.method)) {
            handlers[msg.method](ws, msg);
        }

    });

    ws.on('close', () => {

        if(ws.hasOwnProperty('info'))
        {
            if (ws.info.hasOwnProperty('type'))
            {
                if(ws.info.type === 'driver') {
                    global.USERS.drivers = global.USERS.drivers.filter(driver => {
                        return driver.phone !== ws.info.phone;
                    });

                    server.clients.forEach(client => {
                        client.send(JSON.stringify({
                            action: 'unAuthDriver',
                            data: {
                                count_drivers_online: global.USERS.drivers.length
                            }
                        }));
                    });
                }
                else if (ws.info.type === 'operator') {
                    global.USERS.operators = global.USERS.operators.filter(operator => {
                        if (operator.hasOwnProperty('email')) {
                            return operator.email !== ws.info.email;
                        }
                    });

                    server.clients.forEach(client => {
                        client.send(JSON.stringify({
                            action: 'unAuthOperator',
                            data: {
                                count_operators_online: global.USERS.operators.length
                            }
                        }));

                    });
                }
            }
        }

    });

});